﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestTaskARMO
{
    public partial class FormFileSearcher : Form
    {
        public FormFileSearcher()
        {
            InitializeComponent();
        }

        FolderBrowserDialog fbd = new FolderBrowserDialog();
        DateTime date1 = new DateTime(0, 0);
        private string criterionFilePath = "criterions.txt";
        private volatile bool isPaused = false;
        private volatile bool isStopped = false;


        private void openPathDialogButton_Click(object sender, EventArgs e)
        {
            fbd.ShowNewFolderButton = false;

            if (fbd.ShowDialog() == DialogResult.OK)
            {
                textBoxDirPath.Text = fbd.SelectedPath;
            }
        }

        private async void buttonStartSearch_Click(object sender, EventArgs e)
        {
            timerFileSearcher.Start();

            textBoxDirPath.Enabled = false;
            textBoxNameTamplate.Enabled = false;
            textBoxTextForSearch.Enabled = false;

            buttonStartSearch.Enabled = false;
            buttonPauseSearch.Enabled = true;
            openPathDialogButton.Enabled = false;

            var searchPath =  textBoxDirPath.Text;
            var fileNameTemplate = textBoxNameTamplate.Text;
            var searchingText = textBoxTextForSearch.Text;

            toolStripChekedFiles.Text = ("0");
            listBoxFoundFiles.Items.Clear();
            isPaused = false;
            isStopped = false;

            string[] files = null;
            

            if (fileNameTemplate == "")
                fileNameTemplate = @"*.*";//инициализация переменной шаблона, для того, чтобы поиск происходил, даже при пустом поле

            labelFileInWork.Text = "Поиск файлов с подходящим именем.";
            await Task.Run(() =>  
            {
                try
                {
                    files = Directory.GetFiles(searchPath, fileNameTemplate, SearchOption.AllDirectories);
                    //Thread.Sleep(3000);
                }
                catch
                {
                    //отловить ошибку доступа
                }
            });
            var i = 0;
            var filesChecked = 0;
            string[] foundedFiles = null;

            
            foreach (var file in files)
            {
                labelFileInWork.Text = file.ToString();
                toolStripChekedFiles.Text = (filesChecked + 1).ToString();
               
                await Task.Run(() =>
                {
                    while (isPaused == true)
                    {
                        Thread.Sleep(0);
                        timerFileSearcher.Stop();
                    }
                    if (timerFileSearcher.Enabled == false)
                        timerFileSearcher.Start();
                    StreamReader reading = File.OpenText(file);
                    string str;
                    while ((str = reading.ReadLine()) != null)
                    {
                        //флаг для найденного контента в файле
                        if (str.Contains(searchingText))
                        {
                            i = 1;
                        }
                        else
                        {
                            i = 0;
                        }
                    }


                });
                if (i == 1)
                    listBoxFoundFiles.Items.Add(file);
                filesChecked++;
                if (isStopped == true)
                    break;

                //listBoxFoundFiles.Items.Add(file);
            }

                isStopped = true;
            

            labelFileInWork.Text = "Обработка файлов завершена!";
            endOfSearch();
            timerFileSearcher.Stop();
        }

        private void timerFileSearcher_Tick(object sender, EventArgs e)
        {
            date1 =  date1.AddMilliseconds(10);
            toolStripTimer.Text =  date1.ToString("mm:ss:f");
        }

        private void FormFileSearcher_FormClosing(object sender, FormClosingEventArgs e)
        {
            //сохранение введённых в форму данных при закрытии приложения (в директорию, где лежит exe)
            using (StreamWriter stream = new StreamWriter(Directory.GetCurrentDirectory() + "\\" + criterionFilePath, false))
            {
                stream.WriteLine(textBoxDirPath.Text);
                stream.WriteLine(textBoxNameTamplate.Text);
                stream.WriteLine(textBoxTextForSearch.Text);
            }
        }

        private void FormFileSearcher_Load(object sender, EventArgs e)
        {
            //загрузка критериев (если они были созданы ранее)
            if (File.Exists(criterionFilePath) == true)
            {
                try
                {
                    using (var streamReader = File.OpenText(criterionFilePath))
                    {
                        var lines = streamReader.ReadToEnd()
                            .Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                        textBoxDirPath.Text = lines[0];
                        textBoxNameTamplate.Text = lines[1];
                        textBoxTextForSearch.Text = lines[2];
                        //foreach (var line in lines)
                        // Process line
                    }
                }
                catch
                {

                }
            }
        }

        private void buttonPauseSearch_Click(object sender, EventArgs e)
        {
            if (isPaused == false)
            {
                isPaused = true;
                buttonPauseSearch.Text = "Продолжить";
            }
            else
            {
                isPaused = false;
                buttonPauseSearch.Text = "Пауза";
            }
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            endOfSearch();
            //textBoxDirPath.Enabled = true;
            //textBoxNameTamplate.Enabled = true;
            //textBoxTextForSearch.Enabled = true;

            //buttonStartSearch.Enabled = true;
            //buttonPauseSearch.Enabled = false;
            //openPathDialogButton.Enabled = true;

            //textBoxDirPath.Text = "";
            //textBoxNameTamplate.Text = "";
            //textBoxTextForSearch.Text = "";

            toolStripChekedFiles.Text = ("0");
            listBoxFoundFiles.Items.Clear();

            isStopped = true;
            //isPaused = false;
            buttonPauseSearch.Text = "Пауза";
        }

        private void endOfSearch()
        {
            textBoxDirPath.Enabled = true;
            textBoxNameTamplate.Enabled = true;
            textBoxTextForSearch.Enabled = true;

            buttonStartSearch.Enabled = true;
            buttonPauseSearch.Enabled = false;
            openPathDialogButton.Enabled = true;
        }
    }
}
