﻿namespace TestTaskARMO
{
    partial class FormFileSearcher
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.openPathDialogButton = new System.Windows.Forms.Button();
            this.buttonStartSearch = new System.Windows.Forms.Button();
            this.buttonPauseSearch = new System.Windows.Forms.Button();
            this.buttonClear = new System.Windows.Forms.Button();
            this.listBoxFoundFiles = new System.Windows.Forms.ListBox();
            this.textBoxDirPath = new System.Windows.Forms.TextBox();
            this.textBoxNameTamplate = new System.Windows.Forms.TextBox();
            this.textBoxTextForSearch = new System.Windows.Forms.TextBox();
            this.statusStrip3 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripTimer = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripChekedFiles = new System.Windows.Forms.ToolStripStatusLabel();
            this.timerFileSearcher = new System.Windows.Forms.Timer(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.labelFileInWork = new System.Windows.Forms.Label();
            this.statusStrip3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Критерии для поиска";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Стартовая директория:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Шаблон имени файла:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 106);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(167, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Текст, содержащийся в файле:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(27, 177);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Найденые файлы:";
            // 
            // openPathDialogButton
            // 
            this.openPathDialogButton.Location = new System.Drawing.Point(713, 45);
            this.openPathDialogButton.Name = "openPathDialogButton";
            this.openPathDialogButton.Size = new System.Drawing.Size(75, 23);
            this.openPathDialogButton.TabIndex = 6;
            this.openPathDialogButton.Text = "Обзор";
            this.openPathDialogButton.UseVisualStyleBackColor = true;
            this.openPathDialogButton.Click += new System.EventHandler(this.openPathDialogButton_Click);
            // 
            // buttonStartSearch
            // 
            this.buttonStartSearch.Location = new System.Drawing.Point(32, 143);
            this.buttonStartSearch.Name = "buttonStartSearch";
            this.buttonStartSearch.Size = new System.Drawing.Size(90, 23);
            this.buttonStartSearch.TabIndex = 7;
            this.buttonStartSearch.Text = "Старт";
            this.buttonStartSearch.UseVisualStyleBackColor = true;
            this.buttonStartSearch.Click += new System.EventHandler(this.buttonStartSearch_Click);
            // 
            // buttonPauseSearch
            // 
            this.buttonPauseSearch.Enabled = false;
            this.buttonPauseSearch.Location = new System.Drawing.Point(128, 143);
            this.buttonPauseSearch.Name = "buttonPauseSearch";
            this.buttonPauseSearch.Size = new System.Drawing.Size(90, 23);
            this.buttonPauseSearch.TabIndex = 8;
            this.buttonPauseSearch.Text = "Пауза";
            this.buttonPauseSearch.UseVisualStyleBackColor = true;
            this.buttonPauseSearch.Click += new System.EventHandler(this.buttonPauseSearch_Click);
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(224, 143);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(90, 23);
            this.buttonClear.TabIndex = 9;
            this.buttonClear.Text = "Сброс";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // listBoxFoundFiles
            // 
            this.listBoxFoundFiles.FormattingEnabled = true;
            this.listBoxFoundFiles.Location = new System.Drawing.Point(30, 193);
            this.listBoxFoundFiles.Name = "listBoxFoundFiles";
            this.listBoxFoundFiles.Size = new System.Drawing.Size(677, 160);
            this.listBoxFoundFiles.TabIndex = 10;
            // 
            // textBoxDirPath
            // 
            this.textBoxDirPath.Location = new System.Drawing.Point(152, 48);
            this.textBoxDirPath.Name = "textBoxDirPath";
            this.textBoxDirPath.Size = new System.Drawing.Size(555, 20);
            this.textBoxDirPath.TabIndex = 11;
            // 
            // textBoxNameTamplate
            // 
            this.textBoxNameTamplate.Location = new System.Drawing.Point(152, 73);
            this.textBoxNameTamplate.Name = "textBoxNameTamplate";
            this.textBoxNameTamplate.Size = new System.Drawing.Size(555, 20);
            this.textBoxNameTamplate.TabIndex = 12;
            // 
            // textBoxTextForSearch
            // 
            this.textBoxTextForSearch.Location = new System.Drawing.Point(200, 99);
            this.textBoxTextForSearch.Name = "textBoxTextForSearch";
            this.textBoxTextForSearch.Size = new System.Drawing.Size(507, 20);
            this.textBoxTextForSearch.TabIndex = 13;
            // 
            // statusStrip3
            // 
            this.statusStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripTimer,
            this.toolStripStatusLabel2,
            this.toolStripChekedFiles});
            this.statusStrip3.Location = new System.Drawing.Point(0, 428);
            this.statusStrip3.Name = "statusStrip3";
            this.statusStrip3.Size = new System.Drawing.Size(800, 22);
            this.statusStrip3.TabIndex = 15;
            this.statusStrip3.Text = "statusStrip3";
            this.statusStrip3.UseWaitCursor = true;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(108, 17);
            this.toolStripStatusLabel1.Text = "Времени прошло:";
            // 
            // toolStripTimer
            // 
            this.toolStripTimer.Name = "toolStripTimer";
            this.toolStripTimer.Size = new System.Drawing.Size(43, 17);
            this.toolStripTimer.Text = "00:00:0";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(121, 17);
            this.toolStripStatusLabel2.Text = "Файлов обработано:";
            // 
            // toolStripChekedFiles
            // 
            this.toolStripChekedFiles.Name = "toolStripChekedFiles";
            this.toolStripChekedFiles.Size = new System.Drawing.Size(13, 17);
            this.toolStripChekedFiles.Text = "0";
            // 
            // timerFileSearcher
            // 
            this.timerFileSearcher.Interval = 1;
            this.timerFileSearcher.Tick += new System.EventHandler(this.timerFileSearcher_Tick);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(30, 360);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(133, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Сейчас обрабатывается:";
            // 
            // labelFileInWork
            // 
            this.labelFileInWork.AutoSize = true;
            this.labelFileInWork.Location = new System.Drawing.Point(169, 360);
            this.labelFileInWork.Name = "labelFileInWork";
            this.labelFileInWork.Size = new System.Drawing.Size(0, 13);
            this.labelFileInWork.TabIndex = 18;
            // 
            // FormFileSearcher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.labelFileInWork);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.statusStrip3);
            this.Controls.Add(this.textBoxTextForSearch);
            this.Controls.Add(this.textBoxNameTamplate);
            this.Controls.Add(this.textBoxDirPath);
            this.Controls.Add(this.listBoxFoundFiles);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.buttonPauseSearch);
            this.Controls.Add(this.buttonStartSearch);
            this.Controls.Add(this.openPathDialogButton);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FormFileSearcher";
            this.Text = "TT File Searcher";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormFileSearcher_FormClosing);
            this.Load += new System.EventHandler(this.FormFileSearcher_Load);
            this.statusStrip3.ResumeLayout(false);
            this.statusStrip3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button openPathDialogButton;
        private System.Windows.Forms.Button buttonStartSearch;
        private System.Windows.Forms.Button buttonPauseSearch;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.ListBox listBoxFoundFiles;
        private System.Windows.Forms.TextBox textBoxDirPath;
        private System.Windows.Forms.TextBox textBoxNameTamplate;
        private System.Windows.Forms.TextBox textBoxTextForSearch;
        private System.Windows.Forms.StatusStrip statusStrip3;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripTimer;
        private System.Windows.Forms.Timer timerFileSearcher;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labelFileInWork;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripChekedFiles;
    }
}

