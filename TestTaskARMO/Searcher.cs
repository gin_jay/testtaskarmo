﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TestTaskARMO
{
    class Searcher
    {
        public static async Task<List<string>> SearcherFile (string path, string mask, string text)
        {
            try
            {
                var files = Directory.GetFiles(@path, @mask, SearchOption.AllDirectories);
                List<string> file = new List<string>(files);
                //
                //Thread.Sleep(8000);
                return file;

            }
            catch (Exception e)
            {
                return null;
            }

        }

        //public static void Files (string path, string mask)
        //{
           // var files = Directory.GetFiles(@path, @mask, SearchOption.AllDirectories);
         //   List<string> file = new List<string>(files);
         //   return file;
        //}
    }
}
